#ifndef GLOBALSETTINGS_H
#define GLOBALSETTINGS_H

#include "json.hpp"
#include <QWidget>
#include "ui_GlobalSettings.h"

namespace Ui {
class GlobalSettings;
}

class GlobalSettings : public QWidget
{
    Q_OBJECT

public:
    explicit GlobalSettings(QWidget *parent = nullptr);
    ~GlobalSettings();

private slots:
    void on_fpscapture_valueChanged(int);

private:
    Ui::GlobalSettings *ui;
};

#endif // GLOBALSETTINGS_H
